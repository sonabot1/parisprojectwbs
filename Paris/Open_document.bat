if exist "C:\Program Files\Google\Chrome\Application\" (
	set ChromePath="C:\Program Files\Google\Chrome\Application\chrome.exe"
) else (
	set ChromePath="C:\Program Files (x86)\Google\Chrome\Application\chrome.exe"
)

%ChromePath% --allow-file-access-from-files %1

rem "C:\Program Files\Google\Chrome\Application\chrome.exe" --allow-file-access-from-files %1